#Transactions

Performing multiple operations on a book transactionally
Book should not expose how modifications on it are executed.
A book has to provide methods to change it's title, price, etc. On the database level, these can be modified within
the same query, but the book should not expose this implementation detail.
Hibernate uses code rewriting to cache changes made to objects, and flushes them once the session is flushed.
This is great for optimization, but it undermines OO principles (TODO: research)
Domain objects could expose methods to perform multiple operations at once. These lists of modifications could then
be performed transactionally by the underlying persistence mechanism. Multiple lists of modifications from multiple
domain objects could be grouped together. This would allow a batch of modifications, or transactional operations.
If an operation fails, the transaction should be rolled back, and the error returned to the caller.
What to do with different results of the operations? List of operation-result pairs?

setPrice, setTitle perform individual updates, but they can be used in the same transaction via
book.modify(new BookOperations(updatedPrice, updatedTitle, Optional.EMPTY, updatedStock))
Individual updates should delegate to this method
The BookOperations ctor may get complicated, but this verbosity could be reduced with DSL-like features:
book.modify().price(150).title("New title").stock(0).execute()

For transactions we need to be able to set queryExecutors for existing object
This setter would create race conditions if the object is accessed on multiple threads
Otherwise we need to create a transaction context, put all transaction participants in a list, and use
observable.concat() to execute them sequentially

interface PgTransactionParticipant {
    Observable<Void> execute();
}

#Dirty checking
Is PgBook's handleUpdate OK ? It seems fine, as it doesn't mess with the Book interface, but maybe it could be done
with less coupling. Altough, coupling the book with the book modification operation doesn't seem too bad. Any other
way to have PgBook update it's details? Making details() return an observable and fetching lazily introduces a lot of
messy calls.

#Handling of multiple outcome operations

Operations which can fail could publish two observables, one for successful completion, and one for error cases
(or one for each error case?). The consumer of the result could subscribe to any of them. The implementation shouldn't
start executing on subscription, but rather on a third method call (like execute). If an exception occurs, all of the
observables should emit an error (?).
Pro:
    Gets rid of convoluted (expected)error-or-result types. (Where the error itself probably has multiple meanings)
    No procedural if-else error checking is needed.
Con:
    If the consumer doesn't subscribe to all cases, they could wait indefinitely for the others to occur, therefore
    all consumers would need to be perpared for all outcomes. This could be mitigated by defining a subscriber
    interface per operation. This way the subscriber is aware of all possible operations (maybe even forced to
    implement them).
    If we want transaction handling, there should be one more event that signals any sort of completion.
    Results in more object creation (this should not be an issue).
