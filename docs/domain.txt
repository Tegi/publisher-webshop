The domain
Webshop
Customer
Inventory
Catalog
Book
Cart
Manager
Sale
AccountBook
PaymentProvider

Book
	ISBN (International Standard Book Number), identifies it uniquely
	Title
	Description
	Price

Catalog 
	Is a listing of books available for purchase

Inventory
	Is the stock of all books

Manager
	Is an employee of the store, authorized to conduct the business
A manager can
	Add a book to the inventory, by providing it's details
		The default stock is 0
		The book will be listed in the catalog by default
	Change book's
		Title, description or price
		Changing a book's details while it's in a customer's cart will change the customer's cart's contents. This cas is intentionally not covered by the application
	Remove a book from the catalog
	Change the stock of a book in the inventory
		The stock may not be less than 0
	Consult the account book for a list of sales


Cart
	Is a collection of books that a customer intends to buy

Customer
	A person who visits the store, and intends to buy books
A customer can
	Register on the site
	Authenticate themself to the site
After logging in
	Place items to their cart
	Purchase their cart's contents
		By choosing a shipping option, and providing valid payment details

Sale
	Is information about a customer's purchase
	Time of sale, books and associated quantities, shipping details

AccountBook
	Is the collection of all sales

PaymentProvider
	IS an entity that can safely process a customer's payment details