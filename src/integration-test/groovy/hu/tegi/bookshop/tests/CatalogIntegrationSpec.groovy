package hu.tegi.bookshop.tests

import geb.spock.GebSpec
import hu.tegi.bookshop.BookDetails
import hu.tegi.bookshop.TestTools
import spock.lang.Shared

class CatalogIntegrationSpec extends GebSpec {
    @Shared
    IntegrationTest test
    @Shared
    TestTools testTools

    def setupSpec() {
        this.test = IntegrationTest.INSTANCE;
        test.startApplication()
        testTools = test.testTools
    }

    def setup() {
        testTools.clearDatabase()
    }

    def "successful addition of new book"() {
        setup:
        Map newBook = [isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 0]
        to AddBookPage
        when:
        inputIsFilled(newBook)
        submitButton.click()
        then:
        successMessage.text() == 'Book successfully added'
        when:
        to CatalogPage
        then:
        bookIsInCatalog(newBook)
    }

    def "if a new book request is invalid, it should be displayed"() {
        Map bookWithoutTitle = [isbn: '1234567890123', title: '', description: 'A new book', price: 100]
        to AddBookPage
        when:
        inputIsFilled(bookWithoutTitle)
        submitButton.click()
        then:
        failureMessage.text() == 'Invalid request'
        when:
        to CatalogPage
        then:
        thereIsNoBookWithIsbn(bookWithoutTitle.isbn)
    }

    def "two books with the same ISBN should not be allowed"() {
        String isbn = '1234567890123'
        Map book1 = [isbn: isbn, title: 'Title', description: 'A new book', price: 100]
        Map bookWithSameIsbn = [isbn: isbn, title: 'Title', description: 'A new book', price: 100]
        to AddBookPage
        when:
        inputIsFilled(book1)
        submitButton.click()
        then:
        successMessage.text() == 'Book successfully added'
        when:
        inputIsFilled(bookWithSameIsbn)
        submitButton.click()
        then:
        failureMessage.text() == 'Invalid request'
    }

    def "books' details can be changed after they are added to the catalog"() {
        BookDetails book = new BookDetails('1234567890123', 'Title', 'Description', 100, 0)
        testTools.addBook(book)
        String newTitle = 'Modified title'
        Map modifiedBook = [isbn: '1234567890123', title: newTitle, description: 'A new book', price: 100, stock: 3]
        when:
        to ManagementPage
        editButtonForIsbn(book.isbn).click()
        then:
        at EditBookPage
        when:
        inputIsFilled(modifiedBook)
        submitButton.click()
        then:
        successMessage.text() == 'Book successfully modified'
        when:
        to CatalogPage
        then:
        bookIsInCatalog(modifiedBook)
    }
}