package hu.tegi.bookshop.tests

import geb.spock.GebSpec
import hu.tegi.bookshop.Book
import hu.tegi.bookshop.CartItem
import hu.tegi.bookshop.Sale
import hu.tegi.bookshop.TestTools
import spock.lang.Shared

class CheckoutIntegrationSpec extends GebSpec {
    @Shared
    IntegrationTest test
    @Shared
    TestTools testTools

    def setupSpec() {
        this.test = IntegrationTest.INSTANCE;
        test.startApplication()
        testTools = test.testTools
    }

    def setup() {
        testTools.clearDatabase()
    }

    def "contents of the cart can be purchased"() {
        setup:
        Map bookProps = [isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 1]
        Book book = testTools.addBook(bookProps)
        to CatalogPage
        bookWithIsbnIsAddedToCart(book.details().isbn)
        when:
        cart.checkoutButton.click()
        then:
        at CheckoutShippingPage
        when:
        inputIsFilled([name: 'Jon Doe', postalCode: '1234', city: 'Budapest', address: 'Erzsebet krt. 1'])
        submitButton.click()
        then:
        at CheckoutPaymentPage
        when:
        inputIsFilled([cardNumber: '4908366099900425', expiry: '1014', cvv: '823'])
        submitButton.click()
        Map bookWithLowerStock = [
                isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 0
        ]
        then:
        at CatalogPage
        bookIsInCatalog(bookWithLowerStock)
        //TODO: implement
//        thereIsASaleWithItem(new CartItem(book, 1))
    }

    boolean thereIsASaleWithItem(CartItem cartItem) {
        testTools.listSales().contains { Sale sale ->
            sale.items == [cartItem]
        }
    }
}
