package hu.tegi.bookshop.tests

import geb.Module

class CartModule extends Module {
    static base = { $('#cart') }
    static content = {
        items(required: false) {
            $('tbody tr:not(.total-row)').collect {
                module CartItemModule, it
            }
        }
        total(required: false) {
            items.inject(0, { acc, val -> acc += val.price })
        }
        emptyButton(required: false, to: CatalogPage) {
            $('#emptyCartBtn')
        }
        checkoutButton(required: false, to: CheckoutShippingPage) {
            $('#checkoutBtn')
        }
    }

    boolean hasItem(Map item) {
        items.find { it.matches(item) } != null
    }

    boolean isVisible() {
        //Using groovy truth
        $('#catalog-table')
    }
}
