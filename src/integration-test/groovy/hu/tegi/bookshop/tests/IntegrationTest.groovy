package hu.tegi.bookshop.tests

import grails.boot.GrailsApp
import groovy.transform.CompileStatic
import hu.tegi.Application
import hu.tegi.asyncdb.pg.PooledPgDatabaseContext
import hu.tegi.bookshop.TestTools
import org.springframework.context.ConfigurableApplicationContext

/*
TODO
Singleton might not be the nicest pattern
Maybe there should be a way to detect if the application is running, but it is not started by us
This could be useful to run the tests without restarting the application
 */
@CompileStatic
class IntegrationTest {
    private IntegrationTest() {};
    private boolean running;
    public final static INSTANCE = new IntegrationTest()

    ConfigurableApplicationContext ctx
    PooledPgDatabaseContext dbContext
    TestTools testTools

    void startApplication() {
        if (!running) {
            ctx = GrailsApp.run(Application, new String[0])
            dbContext = ctx.getBean(PooledPgDatabaseContext)
            testTools = new TestTools(dbContext)
        }
        running = true
    }
}
