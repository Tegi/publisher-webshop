package hu.tegi.bookshop.tests

import geb.Module

class CatalogRow extends Module {
    static content = {
        cell { $("td", it) }
        isbn { cell(0).text() }
        title { cell(1).text() }
        description { cell(2).text() }
        price { cell(3).text().toInteger() }
        stock { cell(4).text().toInteger() }
        addButton(to: CatalogPage) { cell(5) }
    }

    boolean matches(Map book) {
        isbn == book.isbn &&
                title == book.title &&
                description == book.description &&
                price == book.price &&
                stock == book.stock
    }
}