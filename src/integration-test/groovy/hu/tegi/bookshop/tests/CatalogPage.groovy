package hu.tegi.bookshop.tests

import geb.Page

class CatalogPage extends Page {
    static url = '/catalog'
    static at = { title == 'Book catalog' }

    static content = {
        books {
            $("#catalog-table tbody tr").collect {
                module CatalogRow, it
            }
        }
        cart(required: false) { module CartModule }
    }

    boolean bookIsInCatalog(Map book) {
        books.find { it.matches(book) } != null
    }

    boolean thereIsNoBookWithIsbn(String isbn) {
        books.find { it.isbn == isbn } == null
    }

    boolean bookWithIsbnIsAddedToCart(String isbn) {
        books.find { it.isbn == isbn }.addButton().click()
    }
}