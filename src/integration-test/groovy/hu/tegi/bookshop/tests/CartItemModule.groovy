package hu.tegi.bookshop.tests

import geb.Module

class CartItemModule extends Module {
    static content = {
        cells { $('td', it) }
        title {
            cells(0).text()
        }
        quantity {
            cells(1).text().toInteger()
        }
        price { cells(2).text().toInteger() }
    }

    boolean matches(Map item) {
        return title == item.title && quantity == item.quantity && price == item.price
    }
}
