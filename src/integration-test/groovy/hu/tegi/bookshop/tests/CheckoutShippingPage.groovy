package hu.tegi.bookshop.tests

import geb.Page

class CheckoutShippingPage extends Page {
    static url = '/checkout/shipping'
    static at = { title == 'Checkout - Shipping' }
    static content = {
        input { $('#checkoutShippingForm') }
        submitButton(to: CheckoutPaymentPage) { $('button[type=submit]') }
    }

    void inputIsFilled(Map address) {
        input.name = address.name
        input.postalCode = address.postalCode
        input.city = address.city
        input.address = address.address
    }
}
