package hu.tegi.bookshop.tests

import geb.Page

class CheckoutPaymentPage extends Page {
    static url = '/checkout/payment'
    static at = { title == 'Checkout - Payment' }
    static content = {
        input { $('#checkoutPaymentForm') }
        submitButton(to: CatalogPage) { $('button[type=submit]') }
    }

    void inputIsFilled(Map paymentInfo) {
        input.cardNumber = paymentInfo.cardNumber
        input.expiry = paymentInfo.expiry
        input.cvv = paymentInfo.cvv
    }
}
