package hu.tegi.bookshop.tests

import geb.Page

class AddBookPage extends Page {
    static url = '/catalog/addBook'
    static at = { title == 'Add a new book' }
    static content = {
        input { $('#addBookForm') }
        submitButton(to: AddBookPage) { $('button[type=submit]') }
        successMessage(required: false) { $('.success-message h2') }
        failureMessage(required: false) { $('.failure-message h2') }
    }

    void inputIsFilled(Map book) {
        input.isbn = book.isbn
        input.title = book.title
        input.description = book.description
        input.price = book.price
    }
}