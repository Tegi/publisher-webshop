package hu.tegi.bookshop.tests

import geb.Page

class EditBookPage extends Page {
    static url = '/catalog/editBook'
    static at = { title == 'Edit a book' }
    static content = {
        input { $('#editBookForm') }
        submitButton(to: EditBookPage) { $('button[type=submit]') }
        successMessage(required: false) { $('.success-message h2') }
        failureMessage(required: false) { $('.failure-message h2') }
    }

    void inputIsFilled(Map book) {
        input.title = book.title
        input.description = book.description
        input.price = book.price
        input.stock = book.stock
    }
}
