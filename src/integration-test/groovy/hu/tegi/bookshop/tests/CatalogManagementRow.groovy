package hu.tegi.bookshop.tests

class CatalogManagementRow {
    static content = {
        cell { $("td", it) }
        isbn { cell(0).text() }
        title { cell(1).text() }
        description { cell(2).text() }
        price { cell(3).text().toInteger() }
        stock { cell(4).text().toInteger() }
        editButton(to: EditBookPage) { cell(5) }
    }
}
