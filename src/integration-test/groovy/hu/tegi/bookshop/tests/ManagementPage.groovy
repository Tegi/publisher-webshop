package hu.tegi.bookshop.tests

import geb.Page
import geb.navigator.Navigator

class ManagementPage extends Page {
    static url = '/catalog/manage'
    static at = { title == 'Catalog management' }

    static content = {
        books {
            $("#management-table tbody tr").collect {
                module CatalogManagementRow, it
            }
        }
    }

    Navigator editButtonForIsbn(String isbn) {
        $(".edit-button[isbn='${isbn}']")
    }
}
