package hu.tegi.bookshop.tests

import geb.spock.GebSpec
import hu.tegi.bookshop.TestTools
import spock.lang.Shared

class CartIntegrationSpec extends GebSpec {
    @Shared
    IntegrationTest test
    @Shared
    TestTools testTools

    def setupSpec() {
        this.test = IntegrationTest.INSTANCE;
        test.startApplication()
        this.testTools = test.testTools
    }

    def setup() {
        testTools.clearDatabase()
    }

    def "by default the cart is not visible"() {
        when:
        to CatalogPage
        then:
        cart.isVisible() == false
    }

    def "a book can be added to the cart"() {
        setup:
        Map book = [isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 0]
        testTools.addBook(book)
        to CatalogPage
        when:
        bookWithIsbnIsAddedToCart(book.isbn)
        then:
        at CatalogPage
        cart.hasItem([title: book.title, quantity: 1, price: book.price])
    }

    def "cart contents are preserved when the user navigates away from the page"() {
        setup:
        Map book = [isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 0]
        testTools.addBook(book)
        to CatalogPage
        when:
        bookWithIsbnIsAddedToCart(book.isbn)
        then:
        cart.hasItem([title: book.title, quantity: 1, price: book.price])
        when:
        to ManagementPage
        to CatalogPage
        then:
        cart.hasItem([title: book.title, quantity: 1, price: book.price])
    }

    def "the cart's contents can be emptied"() {
        setup:
        Map book = [isbn: '1234567890123', title: 'A new book', description: 'A new book', price: 100, stock: 0]
        testTools.addBook(book)
        to CatalogPage
        when:
        bookWithIsbnIsAddedToCart(book.isbn)
        cart.emptyButton.click()
        then:
        cart.isVisible() == false
    }
}
