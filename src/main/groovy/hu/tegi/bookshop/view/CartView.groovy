package hu.tegi.bookshop.view

import hu.tegi.bookshop.CartItem

class CartView {
    List<CartItem> items;

    CartView(List<CartItem> items) {
        this.items = items
    }

    int total() {
        return items.inject(0) { int acc, CartItem item -> acc + item.quantity * item.book.details().price }
    }
}
