package hu.tegi.bookshop.http

import grails.async.Promise
import rx.Observable

import java.util.concurrent.TimeUnit

class ObservablePromise<T> implements Promise<T> {
    Observable<T> observable
    Closure completeClosure

    @Override
    boolean cancel(boolean mayInterruptIfRunning) {
        throw new UnsupportedOperationException()
    }

    @Override
    boolean isCancelled() {
        throw new UnsupportedOperationException()
    }

    @Override
    boolean isDone() {
        throw new UnsupportedOperationException()
    }

    @Override
    T get() throws Throwable {
        throw new UnsupportedOperationException()
    }

    @Override
    T get(long timeout, TimeUnit units) throws Throwable {
        throw new UnsupportedOperationException()
    }

    @Override
    Promise<T> accept(T value) {
        throw new UnsupportedOperationException()
    }

    @Override
    Promise<T> onComplete(Closure callable) {
        completeClosure = callable
        observable.subscribe()
        return this
    }

    @Override
    Promise<T> onError(Closure callable) {
        return this
    }

    @Override
    Promise<T> then(Closure callable) {
        throw new UnsupportedOperationException()
    }
}
