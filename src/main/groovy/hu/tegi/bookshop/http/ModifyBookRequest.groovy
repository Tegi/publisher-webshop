package hu.tegi.bookshop.http

import grails.validation.Validateable

class ModifyBookRequest implements Validateable {
    String title;
    String description;
    Integer price;
    Integer stock;

    static constraits = {
        title nullable: false, blank: false, maxLength: 100
        description nullable: false, blank: false, maxLength: 512
        price nullable: false, min: 1
        price nullable: false, min: 0
    }
}
