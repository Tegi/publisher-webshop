package hu.tegi.bookshop.http

import grails.validation.Validateable

class AddBookRequest implements Validateable {
    String isbn;
    String title;
    String description;
    Integer price;

    static constraits = {
        isbn nullable: false, blank: false, maxLength: 13
        title nullable: false, blank: false, maxLength: 100
        description nullable: false, blank: false, maxLength: 512
        price nullable: false, blank: false, min: 1
    }
}
