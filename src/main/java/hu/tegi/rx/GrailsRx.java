package hu.tegi.rx;

import rx.Observable;
import rx.Observer;
import rx.internal.operators.OperatorDoOnEach;

import javax.servlet.AsyncContext;
import java.util.function.Consumer;

public class GrailsRx {
    public static <T> Observable.Operator<T, T> onNext(AsyncContext ctx, Consumer<T> consumer) {
        return new OperatorDoOnEach<T>(
                new Observer<T>() {
                    @Override
                    public final void onCompleted() {

                    }

                    @Override
                    public final void onError(Throwable e) {
                        //TODO: implement more sophisticated error handling
                        e.printStackTrace();
                    }

                    @Override
                    public final void onNext(T args) {
                        ctx.start(() -> {
                            consumer.accept(args);
                            ctx.dispatch();
                        });
                    }
                }
        );
    }

    public static <T> Observable.Operator<Object, ?> onCompleted(AsyncContext ctx, Runnable runnable) {
        return new OperatorDoOnEach<Object>(
                new Observer<Object>() {
                    @Override
                    public final void onCompleted() {
                        ctx.start(() ->{
                            runnable.run();
                            ctx.dispatch();
                        });
                    }

                    @Override
                    public final void onError(Throwable e) {
                        //TODO: implement more sophisticated error handling
                        e.printStackTrace();
                    }

                    @Override
                    public final void onNext(Object args) {
                    }
                }
        );
    }
}
