package hu.tegi.bookshop;

import rx.Observable;

public interface Ledger {
    Observable<Sale> listSales();
}
