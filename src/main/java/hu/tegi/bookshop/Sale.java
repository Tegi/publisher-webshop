package hu.tegi.bookshop;

import java.time.LocalDateTime;
import java.util.Collection;

public class Sale {
    public final LocalDateTime saleTimestamp;
    public final Collection<CartItem> items;

    public Sale(LocalDateTime saleTimestamp, Collection<CartItem> items) {
        this.saleTimestamp = saleTimestamp;
        this.items = items;
    }
}
