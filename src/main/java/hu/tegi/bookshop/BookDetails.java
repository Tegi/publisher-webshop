package hu.tegi.bookshop;

public class BookDetails {
    public final String isbn;
    public final String title;
    public final String description;
    public final int price;
    public final int stock;

    public BookDetails(String isbn, String title, String description, int price, int stock) {
        this.isbn = isbn;
        this.title = title;
        this.description = description;
        this.price = price;
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookDetails that = (BookDetails) o;

        if (price != that.price) return false;
        if (stock != that.stock) return false;
        if (!isbn.equals(that.isbn)) return false;
        if (!title.equals(that.title)) return false;
        return description.equals(that.description);

    }

    @Override
    public int hashCode() {
        int result = isbn.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + description.hashCode();
        result = 31 * result + price;
        result = 31 * result + stock;
        return result;
    }

    @Override
    public String toString() {
        return "BookDetails{" +
                "isbn='" + isbn + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                '}';
    }
}
