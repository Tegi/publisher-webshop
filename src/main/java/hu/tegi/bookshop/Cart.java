package hu.tegi.bookshop;

import rx.Observable;

/*
TODO
Check stock before adding to cart
Add ability to remove books one by one
 */

public interface Cart {
    String id();

    Observable<CartItem> items();

    Observable<CartOperationError> add(Book book);

    Observable<Void> empty();

    Observable<Void> purchaseContents();
}
