package hu.tegi.bookshop;

import rx.Observable;

public interface ModifyBookOperation {
    ModifyBookOperation setTitle(String title);

    ModifyBookOperation setDescription(String description);

    ModifyBookOperation setPrice(int price);

    ModifyBookOperation setStock(int stock);

    Observable<BookOperationError> execute();
}
