package hu.tegi.bookshop;

import rx.Observable;
import rx.Subscriber;

public class EmptyOrError<T> implements Observable.Operator<T, T> {
    private static class Holder {
        public static final EmptyOrError<?> INSTANCE = new EmptyOrError<>();
    }

    @SuppressWarnings("unchecked")
    public static <T> EmptyOrError<T> instance() {
        return (EmptyOrError<T>) Holder.INSTANCE;
    }


    @Override
    public Subscriber<? super T> call(Subscriber<? super T> child) {
        return new Subscriber<T>() {
            @Override
            public void onCompleted() {
                child.onCompleted();
            }

            @Override
            public void onError(Throwable e) {
                child.onError(e);
            }

            @Override
            public void onNext(T t) {
                child.onError(new IllegalArgumentException("No emissions from this observable were expected"));
            }
        };
    }
}
