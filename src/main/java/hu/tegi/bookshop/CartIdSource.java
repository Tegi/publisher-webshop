package hu.tegi.bookshop;

import java.util.Base64;
import java.util.Random;

public class CartIdSource {
    private final Random random;

    public CartIdSource() {
        random = new Random(System.currentTimeMillis());
    }

    public String generateCartId() {
        byte[] idBytes = new byte[24];
        random.nextBytes(idBytes);
        Base64.Encoder encoder = Base64.getUrlEncoder();
        return encoder.encodeToString(idBytes);
    }
}
