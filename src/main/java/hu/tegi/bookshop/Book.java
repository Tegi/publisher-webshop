package hu.tegi.bookshop;

public interface Book {
    BookDetails details();

    ModifyBookOperation modify();
}
