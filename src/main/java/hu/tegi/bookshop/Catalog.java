package hu.tegi.bookshop;

import rx.Observable;

public interface Catalog {
    Observable<BookOperationError> addBook(BookDetails book);

    BookSearch findBooks();
}
