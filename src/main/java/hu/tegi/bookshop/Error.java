package hu.tegi.bookshop;

public interface Error {
    String name();

    default boolean eq(Error that) {
        if (this == that) {
            return true;
        }
        return this.name().equals(that.name());
    }
}
