package hu.tegi.bookshop;

import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.asyncdb.pg.PooledPgDatabaseContext;
import hu.tegi.bookshop.pg.PgCart;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Webshop {
    private final static String CART_COOKIE_NAME = "cart_id";
    PooledPgDatabaseContext databaseContext;
    CartIdSource cartIdSource;

    public Webshop(PooledPgDatabaseContext databaseContext) {
        this.databaseContext = databaseContext;
        cartIdSource = new CartIdSource();
    }

    public Cart findCart(HttpServletRequest request, HttpServletResponse response) {
        String id = findExistingCartId(request);
        Cart cart;
        if (id == null) {
            id = cartIdSource.generateCartId();
            Cookie cartCookie = new Cookie(CART_COOKIE_NAME, id);
            cartCookie.setMaxAge(60 * 60 * 24 * 365);
            cartCookie.setPath("/");
            response.addCookie(cartCookie);
        }
        return new PgCart(id, databaseContext);
    }

    private String findExistingCartId(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(CART_COOKIE_NAME)) {
                    return c.getValue();
                }
            }
        }
        return null;
    }
}
