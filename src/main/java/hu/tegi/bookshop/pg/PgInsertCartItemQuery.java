package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.CartItem;
import org.jooq.Query;

import static jooq.generated.Tables.CART_ITEM;

public class PgInsertCartItemQuery extends PgAbstractQuery {
    private final String cartId;
    private final CartItem cartItem;

    public PgInsertCartItemQuery(PgDatabaseContext databaseContext, String cartId, CartItem cartItem) {
        super(databaseContext);
        this.cartId = cartId;
        this.cartItem = cartItem;
    }

    @Override
    protected Query getQuery() {
        return dsl.insertInto(CART_ITEM, CART_ITEM.CART_ID, CART_ITEM.ISBN, CART_ITEM.QUANTITY)
                .values(cartId, cartItem.book.details().isbn, cartItem.quantity);
    }
}
