package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.Book;
import hu.tegi.bookshop.BookDetails;
import hu.tegi.bookshop.ModifyBookOperation;

public class PgBook implements Book {
    private BookDetails details;
    private final PgDatabaseContext databaseContext;

    public PgBook(PgDatabaseContext databaseContext, BookDetails details) {
        this.databaseContext = databaseContext;
        this.details = details;
    }

    @Override
    public BookDetails details() {
        return details;
    }

    @Override
    public ModifyBookOperation modify() {
        return new PgModifyBookOperation(databaseContext, this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PgBook)) return false;
        return ((PgBook) o).details.equals(this.details);
    }

    @Override
    public int hashCode() {
        return details.hashCode();
    }
}
