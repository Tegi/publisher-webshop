package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.BookOperationError;
import hu.tegi.bookshop.ModifyBookOperation;
import org.jooq.Query;
import org.jooq.UpdateFinalStep;
import org.jooq.UpdateSetMoreStep;
import org.jooq.UpdateSetStep;
import rx.Observable;

import static jooq.generated.Tables.BOOK;

public class PgModifyBookOperation extends PgAbstractQuery implements ModifyBookOperation {
    private final PgBook book;
    private UpdateSetStep update;
    private UpdateFinalStep finalStep;

    public PgModifyBookOperation(PgDatabaseContext databaseContext, PgBook book) {
        super(databaseContext);
        this.book = book;
        this.update = dsl.update(BOOK);
    }

    @Override
    protected Query getQuery() {
        if (finalStep == null) {
            finalStep = ((UpdateSetMoreStep) update).where(BOOK.ISBN.eq(book.details().isbn));
        }
        return finalStep;
    }

    @Override
    public ModifyBookOperation setTitle(String title) {
        update = update.set(BOOK.TITLE, title);
        return this;
    }

    @Override
    public ModifyBookOperation setDescription(String description) {
        update = update.set(BOOK.DESCRIPTION, description);
        return this;
    }

    @Override
    public ModifyBookOperation setPrice(int price) {
        update = update.set(BOOK.PRICE, price);
        return this;
    }

    @Override
    public ModifyBookOperation setStock(int stock) {
        update = update.set(BOOK.STOCK, stock);
        return this;
    }

    @Override
    public Observable<BookOperationError> execute() {
        return executeQuery().flatMap(rs -> {
            if (rs.updatedRows() != 1) {
                throw new IllegalStateException("Expected to update 1 row, instead updated " + rs.updatedRows());
            } else {
//                return book.handleUpdate().flatMap(r -> Observable.empty());
                return Observable.empty();
            }
        });
    }
}
