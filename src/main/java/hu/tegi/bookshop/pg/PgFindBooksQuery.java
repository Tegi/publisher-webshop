package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.Row;
import hu.tegi.asyncdb.pg.PgAbstractSelect;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.Book;
import hu.tegi.bookshop.BookDetails;
import hu.tegi.bookshop.BookSearch;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import rx.Observable;

import static jooq.generated.Tables.BOOK;

public class PgFindBooksQuery extends PgAbstractSelect implements BookSearch {
    private SelectConditionStep select;
    private final PgDatabaseContext databaseContext;

    public PgFindBooksQuery(PgDatabaseContext databaseContext) {
        super(databaseContext);
        this.databaseContext = databaseContext;
        select = dsl.select().from(BOOK).where(DSL.trueCondition());
    }

    @Override
    protected Select getSelect() {
        return select;
    }

    @Override
    public BookSearch withTitle(String title) {
        select = select.and(BOOK.TITLE.eq(title));
        return this;
    }

    @Override
    public BookSearch withIsbn(String isbn) {
        select = select.and(BOOK.ISBN.eq(isbn));
        return this;
    }

    @Override
    public Observable<Book> find() {
        return fetchRows().map(row -> new PgBook(databaseContext, mapDetails(row)));
    }

    public Observable<BookDetails> findDetails() {
        return fetchRows().map(row -> mapDetails(row));
    }

    private BookDetails mapDetails(Row row) {
        return new BookDetails(
                row.getValue(BOOK.ISBN), row.getValue(BOOK.TITLE), row.getValue(BOOK.DESCRIPTION),
                row.getValue(BOOK.PRICE), row.getValue(BOOK.STOCK)
        );
    }

}
