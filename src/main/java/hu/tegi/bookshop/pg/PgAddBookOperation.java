package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.BookDetails;
import hu.tegi.bookshop.BookOperationError;
import org.jooq.Query;
import rx.Observable;

import static jooq.generated.Tables.BOOK;

public class PgAddBookOperation {
    PgDatabaseContext databaseContext;
    private final BookDetails book;

    public PgAddBookOperation(PgDatabaseContext databaseContext, BookDetails book) {
        this.databaseContext = databaseContext;
        this.book = book;
    }

    public Observable<BookOperationError> execute() {
        return new PgFindBooksQuery(databaseContext).withIsbn(book.isbn).find().isEmpty().flatMap(empty -> {
            if (empty) {
                return new AddBookQuery(databaseContext).executeQuery().flatMap(rs -> Observable.empty());
            } else {
                return Observable.just(BookOperationError.ISBN_NOT_UNIQUE);
            }
        });
    }


    private final class AddBookQuery extends PgAbstractQuery {

        public AddBookQuery(PgDatabaseContext databaseContext) {
            super(databaseContext);
        }

        @Override
        protected Query getQuery() {
            return dsl.insertInto(
                    BOOK, BOOK.ISBN, BOOK.TITLE, BOOK.DESCRIPTION, BOOK.PRICE, BOOK.STOCK
            ).values(
                    book.isbn, book.title, book.description, book.price, book.stock
            );
        }
    }
}
