package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import org.jooq.Query;

import static jooq.generated.Tables.BOOK;
import static jooq.generated.Tables.CART_ITEM;

public class PgDecreaseStockForCartItemsQuery extends PgAbstractQuery {
    private final String cartId;

    public PgDecreaseStockForCartItemsQuery(PgDatabaseContext databaseContext, String cartId) {
        super(databaseContext);
        this.cartId = cartId;
    }

    @Override
    protected Query getQuery() {
        return dsl.update(BOOK)
                .set(BOOK.STOCK, BOOK.STOCK.minus(CART_ITEM.QUANTITY))
                .from(CART_ITEM)
                .where(BOOK.ISBN.eq(CART_ITEM.ISBN))
                .and(CART_ITEM.CART_ID.eq(cartId));
    }
}
