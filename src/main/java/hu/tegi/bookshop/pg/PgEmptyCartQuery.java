package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import org.jooq.Query;

import static jooq.generated.Tables.CART_ITEM;

public class PgEmptyCartQuery extends PgAbstractQuery {
    private final String cartId;

    public PgEmptyCartQuery(PgDatabaseContext databaseContext, String cartId) {
        super(databaseContext);
        this.cartId = cartId;
    }

    @Override
    protected Query getQuery() {
        return dsl.delete(CART_ITEM).where(CART_ITEM.CART_ID.eq(cartId));
    }
}
