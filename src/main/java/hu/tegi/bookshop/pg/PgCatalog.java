package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PooledPgDatabaseContext;
import hu.tegi.bookshop.BookDetails;
import hu.tegi.bookshop.BookOperationError;
import hu.tegi.bookshop.BookSearch;
import hu.tegi.bookshop.Catalog;
import rx.Observable;

public class PgCatalog implements Catalog {
    private final PooledPgDatabaseContext databaseContext;

    public PgCatalog(PooledPgDatabaseContext databaseContext) {
        this.databaseContext = databaseContext;
    }

    @Override
    public Observable<BookOperationError> addBook(BookDetails book) {
        return new PgAddBookOperation(databaseContext, book).execute();
    }

    @Override
    public BookSearch findBooks() {
        return new PgFindBooksQuery(databaseContext);
    }
}
