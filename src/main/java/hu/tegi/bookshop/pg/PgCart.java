package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PooledPgDatabaseContext;
import hu.tegi.bookshop.Book;
import hu.tegi.bookshop.Cart;
import hu.tegi.bookshop.CartItem;
import hu.tegi.bookshop.CartOperationError;
import rx.Observable;

public class PgCart implements Cart {
    private final String id;
    private final PooledPgDatabaseContext databaseContext;

    public PgCart(String id, PooledPgDatabaseContext databaseContext) {
        this.id = id;
        this.databaseContext = databaseContext;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public Observable<CartItem> items() {
        return new PgFindCartItemQuery(databaseContext).withCartId(id).fetch();
    }

    @Override
    public Observable<CartOperationError> add(Book book) {
        return new PgAddCartItemOperation(databaseContext, id, book).execute();
    }

    @Override
    public Observable<Void> empty() {
        return new PgEmptyCartQuery(databaseContext, id).executeQuery().flatMap(r -> Observable.empty());
    }

    @Override
    public Observable<Void> purchaseContents() {
        return databaseContext.startTransaction().flatMap(tCtx ->
                new PgDecreaseStockForCartItemsQuery(tCtx, id).executeQuery().flatMap(decr ->
                        new PgEmptyCartQuery(databaseContext, id).executeQuery()
                                .flatMap(rs -> tCtx.commit())
                                .flatMap(__ -> Observable.empty())
                )
        );
    }
}
