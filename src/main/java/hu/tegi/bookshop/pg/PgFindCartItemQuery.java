package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractSelect;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.CartItem;
import org.jooq.Select;
import org.jooq.SelectConditionStep;
import org.jooq.impl.DSL;
import rx.Observable;

import static jooq.generated.Tables.CART_ITEM;

public class PgFindCartItemQuery extends PgAbstractSelect {
    private final PgDatabaseContext databaseContext;
    private SelectConditionStep<?> select;

    public PgFindCartItemQuery(PgDatabaseContext databaseContext) {
        super(databaseContext);
        this.databaseContext = databaseContext;
        select = dsl.select().from(CART_ITEM).where(DSL.trueCondition());
    }

    public PgFindCartItemQuery withId(int id) {
        select = select.and(CART_ITEM.ID.eq(id));
        return this;
    }

    public PgFindCartItemQuery withIsbn(String isbn) {
        select = select.and(CART_ITEM.ISBN.eq(isbn));
        return this;
    }

    public PgFindCartItemQuery withCartId(String cartId) {
        select = select.and(CART_ITEM.CART_ID.eq(cartId));
        return this;
    }

    public Observable<CartItem> fetch() {
        return fetchRows().flatMap(row -> {
            String isbn = row.getValue(CART_ITEM.ISBN);
            return new PgFindBooksQuery(databaseContext).withIsbn(isbn).find().map(book ->
                    new CartItem(book, row.getValue(CART_ITEM.QUANTITY))
            );
        });
    }

    @Override
    protected Select<?> getSelect() {
        return select;
    }
}
