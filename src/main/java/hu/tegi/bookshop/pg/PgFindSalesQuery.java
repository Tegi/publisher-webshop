package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.Sale;
import org.jooq.Query;
import rx.Observable;

public class PgFindSalesQuery extends PgAbstractQuery {
    public PgFindSalesQuery(PgDatabaseContext databaseContext) {
        super(databaseContext);
    }

    @Override
    protected Query getQuery() {
        throw new UnsupportedOperationException();
    }

    public Observable<Sale> fetch() {
        throw new UnsupportedOperationException();
    }
}
