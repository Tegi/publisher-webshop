package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgAbstractQuery;
import hu.tegi.asyncdb.pg.PgDatabaseContext;
import org.jooq.Query;

import static jooq.generated.Tables.CART_ITEM;

public class PgUpdateCartItemQuery extends PgAbstractQuery {
    private final String isbn;
    private final String cartId;
    private final int quantity;

    public PgUpdateCartItemQuery(PgDatabaseContext databaseContext, String isbn, String cartId, int quantity) {
        super(databaseContext);
        this.isbn = isbn;
        this.cartId = cartId;
        this.quantity = quantity;
    }

    @Override
    protected Query getQuery() {
        return dsl.update(CART_ITEM)
                .set(CART_ITEM.QUANTITY, quantity)
                .where(CART_ITEM.ISBN.eq(isbn)).and(CART_ITEM.CART_ID.eq(cartId));
    }

}
