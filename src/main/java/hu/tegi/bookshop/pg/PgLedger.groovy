package hu.tegi.bookshop.pg

import hu.tegi.bookshop.Ledger
import hu.tegi.bookshop.Sale
import rx.Observable

class PgLedger implements Ledger {
    @Override
    Observable<Sale> listSales() {
        throw new UnsupportedOperationException()
    }
}
