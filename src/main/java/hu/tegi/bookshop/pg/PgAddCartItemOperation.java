package hu.tegi.bookshop.pg;

import hu.tegi.asyncdb.pg.PgDatabaseContext;
import hu.tegi.bookshop.Book;
import hu.tegi.bookshop.CartItem;
import hu.tegi.bookshop.CartOperationError;
import rx.Observable;

public class PgAddCartItemOperation {
    private final PgDatabaseContext databaseContext;
    private final String cartId;
    private final Book book;

    public PgAddCartItemOperation(PgDatabaseContext databaseContext, String cartId, Book book) {
        this.databaseContext = databaseContext;
        this.cartId = cartId;
        this.book = book;
    }

    public Observable<CartOperationError> execute() {
        return new PgFindCartItemQuery(databaseContext).withCartId(cartId).withIsbn(book.details().isbn).fetch()
                .firstOrDefault(null).flatMap(existingItem -> {
                    if (existingItem == null) {
                        return new PgInsertCartItemQuery(databaseContext, cartId, new CartItem(book, 1))
                                .executeQuery().flatMap(r -> Observable.empty());
                    } else {
                        return new PgUpdateCartItemQuery(
                                databaseContext, existingItem.book.details().isbn, cartId, existingItem.quantity + 1
                        ).executeQuery().flatMap(r -> Observable.empty());
                    }
                });
    }
}
