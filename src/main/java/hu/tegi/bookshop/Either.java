package hu.tegi.bookshop;

public class Either<L, R> {
    public final R right;
    public final L left;

    public Either(L left, R right) {
        if (right == null && left == null) {
            throw new IllegalArgumentException("Either left or right must not be null");
        }
        this.right = right;
        this.left = left;
    }

    static <L, R> Either<L, R> right(R right) {
        return new Either<L, R>(null, right);
    }

    static <L, R> Either<L, R> left(L left) {
        return new Either<L, R>(left, null);
    }

    boolean isRight() {
        return right != null;
    }

    boolean isLeft() {
        return left != null;
    }
}
