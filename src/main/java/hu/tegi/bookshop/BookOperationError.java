package hu.tegi.bookshop;

public enum BookOperationError implements Error {
    ISBN_NOT_UNIQUE
}
