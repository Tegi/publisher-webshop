package hu.tegi.bookshop;

import rx.Observable;

public interface BookSearch {
    BookSearch withTitle(String title);

    BookSearch withIsbn(String isbn);

    Observable<Book> find();

}
