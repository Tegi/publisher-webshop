package hu.tegi.asyncdb.pg;

import hu.tegi.asyncdb.AsyncSelect;
import hu.tegi.asyncdb.ResultSet;
import hu.tegi.asyncdb.Row;
import org.jooq.Query;
import org.jooq.Select;
import rx.Observable;

public abstract class PgAbstractSelect extends PgAbstractQuery implements AsyncSelect {
    public PgAbstractSelect(PgDatabaseContext databaseContext) {
        super(databaseContext);
    }

    @Override
    public Observable<ResultSet> fetchSet() {
        return queryExecutor.querySet(getSql(), getBindValues())
                .map(rs -> new PgJooqResultSetWrapper(getSelect().fields(), rs));
    }

    @Override
    public Observable<Row> fetchRows() {
        return queryExecutor.queryRows(getSql(), getBindValues())
                .map(row -> new PgJooqRow(getSelect().fields(), row));
    }

    @Override
    protected Query getQuery() {
        return getSelect();
    }

    protected abstract Select<?> getSelect();
}
