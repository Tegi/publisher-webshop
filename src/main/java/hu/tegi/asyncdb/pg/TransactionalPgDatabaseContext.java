package hu.tegi.asyncdb.pg;

import com.github.pgasync.QueryExecutor;
import com.github.pgasync.Transaction;
import org.jooq.DSLContext;
import rx.Observable;

public class TransactionalPgDatabaseContext implements PgDatabaseContext {
    private final Transaction transaction;
    private final DSLContext dslContext;

    public TransactionalPgDatabaseContext(Transaction transaction, DSLContext dslContext) {
        this.transaction = transaction;
        this.dslContext = dslContext;
    }

    @Override
    public DSLContext getDslContext() {
        return dslContext;
    }

    @Override
    public QueryExecutor getQueryExecutor() {
        return transaction;
    }

    public Observable<Void> commit() {
        return transaction.commit();
    }

    public Observable<Void> rollback() {
        return transaction.rollback();
    }
}
