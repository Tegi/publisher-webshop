package hu.tegi.asyncdb.pg;

import hu.tegi.asyncdb.ResultSet;
import hu.tegi.asyncdb.Row;
import org.jooq.Field;

import java.util.Iterator;

public class PgJooqResultSetWrapper implements ResultSet {
    private final Field[] fields;
    private final com.github.pgasync.ResultSet resultSet;

    public PgJooqResultSetWrapper(Field[] fields, com.github.pgasync.ResultSet resultSet) {
        this.fields = fields;
        this.resultSet = resultSet;
    }

    @Override
    public Field[] fields() {
        return fields;
    }

    @Override
    public Row get(int i) {
        return new PgJooqRow(fields, resultSet.row(i));
    }

    @Override
    public int size() {
        return resultSet.size();
    }

    @Override
    public int updatedRows() {
        return resultSet.updatedRows();
    }

    @Override
    public Iterator<Row> iterator() {
        return new PgJooqResultSetIterator();
    }

    private class PgJooqResultSetIterator implements Iterator<Row> {
        private final Iterator<com.github.pgasync.Row> iterator;

        public PgJooqResultSetIterator() {
            iterator = resultSet.iterator();
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public Row next() {
            return new PgJooqRow(fields, iterator.next());
        }
    }
}
