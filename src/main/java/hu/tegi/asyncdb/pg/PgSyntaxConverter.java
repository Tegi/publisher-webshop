package hu.tegi.asyncdb.pg;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PgSyntaxConverter {
    public String convertToPgSqlSyntax(String sql) {
        Matcher matcher = Pattern.compile("\\?").matcher(sql);
        boolean result = matcher.find();
        if (result) {
            StringBuffer sb = new StringBuffer();
            int idx = 1;
            do {
                matcher.appendReplacement(sb, "\\$" + idx);
                idx++;
                result = matcher.find();
            } while (result);
            matcher.appendTail(sb);
            return sb.toString();
        } else {
            return sql;
        }
    }
}
