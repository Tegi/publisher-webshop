package hu.tegi.asyncdb.pg;

import com.github.pgasync.QueryExecutor;
import org.jooq.DSLContext;

public interface PgDatabaseContext {
    DSLContext getDslContext();
    QueryExecutor getQueryExecutor();
}
