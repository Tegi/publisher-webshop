package hu.tegi.asyncdb.pg;

import com.github.pgasync.QueryExecutor;
import hu.tegi.asyncdb.AsyncQuery;
import hu.tegi.asyncdb.ResultSet;
import org.jooq.DSLContext;
import org.jooq.Query;
import rx.Observable;

public abstract class PgAbstractQuery implements AsyncQuery {
    protected final DSLContext dsl;
    protected final QueryExecutor queryExecutor;
    protected final PgSyntaxConverter pgSyntaxConverter = new PgSyntaxConverter();

    public PgAbstractQuery(PgDatabaseContext databaseContext) {
        this.dsl = databaseContext.getDslContext();
        this.queryExecutor = databaseContext.getQueryExecutor();
    }

    protected abstract Query getQuery();

    protected String getSql() {
        return pgSyntaxConverter.convertToPgSqlSyntax(getQuery().getSQL());
    }

    protected Object[] getBindValues() {
        return getQuery().getBindValues().toArray();
    }

    @Override
    public Observable<ResultSet> executeQuery() {
        return queryExecutor.querySet(getSql(), getBindValues())
                .map(rs -> new PgJooqResultSetWrapper(null, rs));
    }
}
