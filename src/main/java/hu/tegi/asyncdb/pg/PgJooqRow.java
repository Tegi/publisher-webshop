package hu.tegi.asyncdb.pg;

import hu.tegi.asyncdb.Row;
import org.jooq.Field;

public class PgJooqRow implements Row {
    private final Field<?>[] fields;
    private final com.github.pgasync.Row pgRow;

    public PgJooqRow(Field<?>[] fields, com.github.pgasync.Row pgRow) {
        this.fields = fields;
        this.pgRow = pgRow;
    }

    @Override
    public <T> T getValue(Field<T> field) {
        Class<?> targetType = field.getType();
        String fieldName = field.getName();
        if (String.class.equals(targetType)) {
            return (T) pgRow.getString(fieldName);
        } else if (Integer.class.equals(targetType)) {
            return (T) pgRow.getInt(fieldName);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private int indexOrFail(Field<?> field) {
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].equals(field)) {
                return i;
            }
        }
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].getName().equals(field.getName())) {
                return i;
            }
        }
        throw new IllegalArgumentException("Field (" + field + ") is not contained in Row");
    }
}
