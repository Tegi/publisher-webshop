package hu.tegi.asyncdb.pg;

import com.github.pgasync.ConnectionPool;
import com.github.pgasync.QueryExecutor;
import org.jooq.DSLContext;
import rx.Observable;

public class PooledPgDatabaseContext implements PgDatabaseContext {
    private final DSLContext dslContext;
    private final ConnectionPool connectionPool;

    public PooledPgDatabaseContext(DSLContext dslContext, ConnectionPool connectionPool) {
        this.dslContext = dslContext;
        this.connectionPool = connectionPool;
    }

    @Override
    public DSLContext getDslContext() {
        return dslContext;
    }

    @Override
    public QueryExecutor getQueryExecutor() {
        return connectionPool;
    }

    public Observable<TransactionalPgDatabaseContext> startTransaction() {
        return connectionPool.getConnection()
                .flatMap(con -> con.begin())
                .map(t -> new TransactionalPgDatabaseContext(t, dslContext));
    }
}
