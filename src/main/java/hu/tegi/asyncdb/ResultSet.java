package hu.tegi.asyncdb;

import org.jooq.Field;

public interface ResultSet extends Iterable<Row> {
    Field[] fields();

    Row get(int i);

    int size();

    int updatedRows();
}
