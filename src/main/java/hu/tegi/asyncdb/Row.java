package hu.tegi.asyncdb;

import org.jooq.Field;

public interface Row {
    public <T> T getValue(Field<T> field);
}
