package hu.tegi.asyncdb;

import rx.Observable;

public interface AsyncSelect extends AsyncQuery {
    Observable<ResultSet> fetchSet();

    Observable<Row> fetchRows();
}
