package hu.tegi.asyncdb;

import rx.Observable;

public interface AsyncQuery {
    Observable<ResultSet> executeQuery();
}
