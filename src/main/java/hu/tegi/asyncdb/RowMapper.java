package hu.tegi.asyncdb;

public interface RowMapper<T, U> {
    U map(T row);
}
