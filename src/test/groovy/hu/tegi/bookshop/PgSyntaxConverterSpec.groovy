package hu.tegi.bookshop

import hu.tegi.asyncdb.pg.PgSyntaxConverter
import spock.lang.Specification

class PgSyntaxConverterSpec extends Specification {
    def "Replaces question marks in standard SQL queries with pg-specific \$arg markers"() {
        setup:
        PgSyntaxConverter converter = new PgSyntaxConverter();
        expect:
        converter.convertToPgSqlSyntax("select * from book where isbn = ? and title = ?") ==
                "select * from book where isbn = \$1 and title = \$2"
    }
}
