package hu.tegi.bookshop

import com.github.pgasync.ConnectionPool
import com.github.pgasync.ConnectionPoolBuilder
import groovy.transform.CompileStatic
import hu.tegi.asyncdb.pg.PooledPgDatabaseContext
import hu.tegi.bookshop.pg.PgAddBookOperation
import hu.tegi.bookshop.pg.PgFindBooksQuery
import hu.tegi.bookshop.pg.PgFindSalesQuery
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL

@CompileStatic
class TestTools {
    PooledPgDatabaseContext dbContext

    TestTools() {
        ConnectionPoolBuilder builder = new ConnectionPoolBuilder()
        ConnectionPool pool = builder.hostname('127.0.0.1')
                .username('postgres')
                .password('postgres')
                .database('publisher_webshop')
                .build()
        DSLContext dslContext = DSL.using(SQLDialect.POSTGRES_9_5)
        dbContext = new PooledPgDatabaseContext(dslContext, pool)
    }

    TestTools(PooledPgDatabaseContext dbContext) {
        this.dbContext = dbContext
    }

    void clearDatabase() {
        dbContext.queryExecutor.querySet("delete from webshop.cart_item").toBlocking().first();
        dbContext.queryExecutor.querySet("delete from webshop.book").toBlocking().first();
    }

    Book findBookByIsbn(String isbn) {
        new PgFindBooksQuery(dbContext).withIsbn(isbn).find().toBlocking().single()
    }

    Book addBook(BookDetails book) {
        new PgAddBookOperation(dbContext, book).execute().toBlocking().subscribe()
        findBookByIsbn(book.isbn)
    }

    Book addBook(Map book) {
        addBook(
                new BookDetails(
                        (String) book.isbn, (String) book.title, (String) book.description, (int) book.price,
                        (int) book.stock
                )
        )
    }

    Collection<Sale> listSales() {
        new PgFindSalesQuery(dbContext).fetch().toList().toBlocking().first()
    }
}
