package hu.tegi.bookshop

import hu.tegi.bookshop.pg.PgCart
import spock.lang.Shared
import spock.lang.Specification

class CartSpec extends Specification {
    @Shared
    TestTools testTools
    @Shared
    Cart cart

    def setupSpec() {
        testTools = new TestTools()
        cart = new PgCart('1', testTools.dbContext)
    }

    def setup() {
        testTools.clearDatabase()
    }

    def "an item can be added to the cart"() {
        setup:
        BookDetails details = new BookDetails('1234567890123', 'Title', 'Details', 100, 1)
        Book book = testTools.addBook(details)
        when:
        cart.add(book).lift(EmptyOrError.instance()).toBlocking().subscribe()
        then:
        cart.items().toList().toBlocking().first() == [new CartItem(book, 1)]
    }

    def "quantity is increased when an item that is already present is added"() {
        setup:
        BookDetails details = new BookDetails('1234567890123', 'Title', 'Details', 100, 1)
        Book book = testTools.addBook(details)
        when:
        cart.add(book).concatWith(cart.add(book)).lift(EmptyOrError.instance()).toBlocking().subscribe()
        then:
        cart.items().toList().toBlocking().first() == [new CartItem(book, 2)]
    }

    def "the cart can be emptied"() {
        setup:
        BookDetails details = new BookDetails('1234567890123', 'Title', 'Details', 100, 1)
        Book book = testTools.addBook(details)
        cart.add(book).toBlocking().subscribe()
        when:
        cart.empty().toBlocking().subscribe()
        then:
        cart.items().toList().toBlocking().first() == Collections.EMPTY_LIST
    }

    def "after items are purchased, the cart is empty"() {
        BookDetails details = new BookDetails('1234567890123', 'Title', 'Details', 100, 1)
        Book book = testTools.addBook(details)
        cart.add(book).toBlocking().subscribe()
        when:
        cart.purchaseContents().toBlocking().subscribe()
        then:
        cart.items().isEmpty().toBlocking().single() == true
    }

    def "after items are purchased, their stock is decreased"() {
        BookDetails details = new BookDetails('1234567890123', 'Title', 'Details', 100, 1)
        Book book = testTools.addBook(details)
        cart.add(book).toBlocking().subscribe()
        when:
        cart.purchaseContents().toBlocking().subscribe()
        then:
        testTools.findBookByIsbn(details.isbn).details() == new BookDetails('1234567890123', 'Title', 'Details', 100, 0)
    }
}
