package hu.tegi.bookshop

import groovy.transform.TypeChecked
import hu.tegi.bookshop.pg.PgCatalog
import spock.lang.Shared
import spock.lang.Specification

@TypeChecked
class CatalogSpec extends Specification {
    @Shared
    TestTools testTools
    @Shared
    Catalog catalog

    def setupSpec() {
        testTools = new TestTools()
        catalog = new PgCatalog(testTools.dbContext)
    }

    def setup() {
        testTools.clearDatabase()
    }

    def "books added by addBook can later be fetched"() {
        setup:
        BookDetails newBook = new BookDetails('1234567890123', 'A new book', 'A new book', 100, 0)
        when:
        catalog.addBook(newBook).lift(EmptyOrError.instance()).toBlocking().subscribe()
        then:
        catalog.findBooks().find().toBlocking().single().details() == newBook
    }

    def "books can be searched for by title"() {
        setup:
        String theTitle = 'The title'
        BookDetails theBook = new BookDetails('1234567890123', theTitle, 'A new book', 100, 0)
        catalog.addBook(theBook).lift(EmptyOrError.instance()).toBlocking().subscribe()
        expect:
        catalog.findBooks().withTitle(theTitle).find().toBlocking().single().details() == theBook
    }

    def "Two books with the same ISBN may not be present in the catalog"() {
        setup:
        String isbn = '1234567890123'
        BookDetails firstBook = new BookDetails(isbn, "Title", 'A new book', 100, 0)
        BookDetails bookWithSameIsbn = new BookDetails(isbn, "Title2", 'Description2', 200, 0)
        catalog.addBook(firstBook).lift(EmptyOrError.instance()).toBlocking().subscribe()
        expect:
        catalog.addBook(bookWithSameIsbn).toBlocking().single() == BookOperationError.ISBN_NOT_UNIQUE

    }

    def "Books can be modified"() {
        setup:
        BookDetails bookDetails = new BookDetails('1234567890123', 'Title', 'Description', 100, 0)
        BookDetails modified = new BookDetails('1234567890123', 'New title', 'New description', 200, 10)
        catalog.addBook(bookDetails).lift(EmptyOrError.instance()).toBlocking().subscribe()
        Book book = testTools.findBookByIsbn(bookDetails.isbn)
        when:
        book.modify().setTitle(modified.title).setDescription(modified.description).setPrice(modified.price)
                .setStock(modified.stock).execute().lift(EmptyOrError.instance()).toBlocking().subscribe()
        then:
        testTools.findBookByIsbn(bookDetails.isbn).details() == modified
    }


}
