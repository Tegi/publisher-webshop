package rx

import groovy.transform.CompileStatic
import spock.lang.Ignore
import spock.lang.Specification

@CompileStatic
@Ignore
class MergeSpec extends Specification {
    def "Merged observable completes when both observables have completed"() {
        setup:
        Observable first;
        Observable second;
        Set<Subscriber> firstSubscribers = [] as Set
        Set<Subscriber> secondSubscribers = [] as Set
        first = Observable.create({ Subscriber o ->
            firstSubscribers << o
            startThread(500, firstSubscribers)
        } as Observable.OnSubscribe).doOnCompleted({ println "first subscriber oncomplete" })
        second = Observable.create({ Subscriber o ->
            secondSubscribers << o
            startThread(1000, secondSubscribers)
        } as Observable.OnSubscribe).doOnCompleted({ println "second subscriber oncomplete" })
        /*
        first = Observable.timer(500, TimeUnit.MILLISECONDS).doOnCompleted({println "first completed"})
        second = Observable.timer(1000, TimeUnit.MILLISECONDS).doOnCompleted({println "second completed"})
        */
        Observable merged = Observable.merge(first, second)
        merged.toBlocking().subscribe()
        println "merged completed"
        expect:
        true
    }

    def startThread(long delay, Set<Subscriber> subscribers) {
        Thread t = {
            Thread.sleep(delay);
            subscribers.each { Subscriber s -> s.onCompleted() }
        } as Thread
        t.start()
    }
}
