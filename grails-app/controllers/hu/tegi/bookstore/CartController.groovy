package hu.tegi.bookstore

import grails.compiler.GrailsCompileStatic
import hu.tegi.bookshop.Book
import hu.tegi.bookshop.Cart
import hu.tegi.bookshop.Catalog
import hu.tegi.bookshop.Webshop
import hu.tegi.rx.GrailsRx

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@GrailsCompileStatic
class CartController {
    Webshop webshop
    Catalog catalog

    def add() {
        assert params.isbn
        Cart cart = webshop.findCart(request as HttpServletRequest, response as HttpServletResponse)
        catalog.findBooks().withIsbn((String) params.isbn).find().single().flatMap({ Book book ->
            cart.add(book)
        }).lift(GrailsRx.onCompleted(startAsync(), {
            redirect controller: 'catalog'
        })).subscribe()
    }

    def empty() {
        Cart cart = webshop.findCart(request as HttpServletRequest, response as HttpServletResponse)
        cart.empty().lift(GrailsRx.onCompleted(startAsync(), {
            redirect controller: 'catalog'
        })).subscribe()
    }
}
