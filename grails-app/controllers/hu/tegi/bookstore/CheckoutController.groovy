package hu.tegi.bookstore

import grails.compiler.GrailsCompileStatic
import hu.tegi.bookshop.Webshop
import hu.tegi.rx.GrailsRx

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@GrailsCompileStatic
class CheckoutController {
    Webshop webshop

    def shipping() {
        if (request.get) {
            render view: '/checkout/shipping'
        } else {
            redirect controller: 'checkout', action: 'payment'
        }
    }

    def payment() {
        if (request.get) {
            render view: '/checkout/payment'
        } else {
            webshop.findCart((HttpServletRequest) request, (HttpServletResponse) response).purchaseContents()
                    .lift(GrailsRx.onCompleted(startAsync(), {
                redirect controller: 'catalog'
            })).subscribe()
        }
    }
}
