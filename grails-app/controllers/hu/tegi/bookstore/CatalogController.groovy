package hu.tegi.bookstore

import hu.tegi.bookshop.Book
import hu.tegi.bookshop.BookDetails
import hu.tegi.bookshop.BookOperationError
import hu.tegi.bookshop.Catalog
import hu.tegi.bookshop.http.AddBookRequest
import hu.tegi.bookshop.http.ModifyBookRequest
import hu.tegi.rx.GrailsRx
import rx.Observable

import javax.servlet.AsyncContext

class CatalogController {
    Catalog catalog

    def addBook() {
        if (request.get) {
            return render(view: '/catalog/addBook')
        } else {
            AddBookRequest request = new AddBookRequest()
            bindData(request, params)
            if (!request.validate()) {
                return render(view: '/catalog/addBook', model: [failure: true])
            } else {
                catalog.addBook(
                        new BookDetails(request.isbn, request.title, request.description, request.price, 0)
                ).singleOrDefault(null).lift(GrailsRx.onNext(startAsync(), { BookOperationError error ->
                    if (error == null) {
                        render(view: '/catalog/addBook', model: [success: true])
                    } else {
                        render(view: '/catalog/addBook', model: [failure: true])
                    }
                })).subscribe()
            }
        }
    }

    def index() {
        catalog.findBooks().find().map({ Book book -> book.details() })
                .toList().lift(GrailsRx.onNext(startAsync(), {
            List<BookDetails> books ->
                render(view: "/catalog/index", model: [books: books])
        })).subscribe();
    }

    def manage() {
        catalog.findBooks().find().map({ Book book -> book.details() }).toList()
                .lift(GrailsRx.onNext(startAsync(), { List<Book> books ->
            render view: '/catalog/manage', model: [books: books]
        })).subscribe()
    }

    def edit() {
        Object stringParam = params.isbn
        String isbn = stringParam instanceof List ? stringParam[0] : stringParam
        Observable<Book> findBook = catalog.findBooks().withIsbn(isbn).find()
        AsyncContext ctx = startAsync()
        if (request.get) {
            findBook.lift(GrailsRx.onNext(ctx, { Book book ->
                render view: '/catalog/editBook', model: [book: book.details()]
            })).subscribe()
        } else {
            ModifyBookRequest mod = new ModifyBookRequest()
            //if this is invoked in a callback, that is not in FinishGrailsRequest,
            //an error will be thrown, as bindData uses the current request
            //if binddata was in flatMap callback, whole thing would blow up. Testing saved the day
            bindData(mod, params)
            findBook.flatMap({ Book book ->
                book.modify().setTitle(mod.title).setDescription(mod.description)
                        .setPrice(mod.price).setStock(mod.stock).execute().singleOrDefault(null)
                        .flatMap({ BookOperationError e ->
                    Observable.just(new BookDetails(isbn, mod.title, mod.description, mod.price, mod.stock))
                })
            }).lift(GrailsRx.onNext(ctx, { BookDetails details ->
                render view: '/catalog/editBook', model: [book: details, success: true]
            })).subscribe()
        }
    }

}
