package hu.tegi

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: "catalog", action: "index")
        "500"(view: "/error")
        "404"(view: "/notFound")
    }
}
