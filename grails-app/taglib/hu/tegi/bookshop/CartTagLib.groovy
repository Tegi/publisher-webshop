package hu.tegi.bookshop

import hu.tegi.bookshop.view.CartView

class CartTagLib {
    Webshop webshop

    static namespace = 'cart'
    static defaultEncodeAs = [taglib: 'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    def renderCart = { attrs, body ->
        Cart cart = webshop.findCart(request, response)
        List<CartItem> items = cart.items().toList().toBlocking().first()
        if (!items.isEmpty()) {
            out << g.render(template: '/cart/cart', model: [cart: new CartView(items)])
        }
    }
}
