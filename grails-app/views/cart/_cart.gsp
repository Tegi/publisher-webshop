<section id="cart">
    <div class="row cart-title">
        <h2 class="six columns">Cart contents</h2>
        <a id="emptyCartBtn" href="${createLink(controller:'cart', action:'empty')}" class="button three columns">
            Empty cart
        </a>
        <a id="checkoutBtn" href="${createLink(controller:'checkout', action:'shipping')}" class="button three columns">
            Checkout
        </a>
    </div>
    <table>
        <thead>
        <tr>
            <th>Title</th>
            <th>Qty</th>
            <th class="price-col">Price</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${cart.items}">
            <tr>
                <td>${it.book.details().title}</td>
                <td>${it.quantity}</td>
                <td class="price-col">${it.book.details().price}</td>
            </tr>
        </g:each>
        <tr class="total-row">
            <td>Total</td>
            <td colspan="2" class="price-col">${cart.total()}</td>
        </tr>
        </tbody>
    </table>
</section>