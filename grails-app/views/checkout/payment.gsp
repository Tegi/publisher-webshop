<head>
    <meta name="layout" content="main"/>
    <title>Checkout - Payment</title>
</head>
<body>
<h1>Checkout - Payment</h1>
<form id="checkoutPaymentForm" method="post" action="${createLink(controller:'checkout', action:'payment')}">
    <label for="cardNumber">Card number</label>
    <input type="text" name="cardNumber" id="cardNumber"/>
    <label for="expiry">Expiry</label>
    <input type="text" name="expiry" id="expiry"/>
    <label for="cvv">CVV</label>
    <input type="text" name="cvv" id="cvv"/>
    <div class="row">
        <button type="submit">Submit</button>
    </div>
</form>
</body>