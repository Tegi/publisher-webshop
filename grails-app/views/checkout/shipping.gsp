<head>
    <meta name="layout" content="main"/>
    <title>Checkout - Shipping</title>
</head>
<body>
<h1>Checkout - Shipping</h1>
<form id="checkoutShippingForm" method="post" action="${createLink(controller:'checkout', action:'shipping')}">
    <label for="name">Name</label>
    <input type="text" name="name" id="name"/>
    <label for="postalCode">Postal code</label>
    <input type="text" name="postalCode" id="postalCode"/>
    <label for="city">City</label>
    <input type="text" name="city" id="city"/>
    <label for="address">Address</label>
    <input type="text" name="address" id="address"/>
    <div class="row">
        <button type="submit">Submit</button>
    </div>
</form>
</body>