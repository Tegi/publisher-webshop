<head>
    <meta name="layout" content="main"/>
    <title>Catalog management</title>
</head>
<body>
<h1>Catalog</h1>
<table id="management-table" class="u-full-width">
    <thead>
    <tr>
        <th>ISBN</th>
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
        <th>Stock</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${books}">
        <tr>
            <td>${it.isbn}</td>
            <td>${it.title}</td>
            <td>${it.description}</td>
            <td>${it.price}</td>
            <td>${it.stock}</td>
            <td>
                <a href="${createLink(action:'edit', params:[isbn: it.isbn])}" isbn="${it.isbn}"
                   class="button edit-button">Edit</a>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>
</body>