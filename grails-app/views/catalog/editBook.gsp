<head>
    <meta name="layout" content="main"/>
    <title>Edit a book</title>
</head>
<body>
<h1>Edit a book</h1>
<g:if test="${success}">
    <div class="success-message">
        <h2>Book successfully modified</h2>
    </div>
</g:if>
<g:if test="${failure}">
    <div class="failure-message">
        <h2>Invalid request</h2>
    </div>
</g:if>
<form id="editBookForm" method="post">
    <label for="isbn">ISBN</label>
    <input type="text" name="isbn" id="isbn" value="${book.isbn}" disabled/>
    <label for="title">Title</label>
    <input type="text" name="title" id="title" value="${book.title}"/>
    <label for="description">Description</label>
    <input type="text" name="description" id="description" value="${book.description}"/>
    <label for="price">Price</label>
    <input type="text" name="price" id="price" value="${book.price}"/>
    <label for="stock">Stock</label>
    <input type="text" name="stock" id="stock" value="${book.stock}"/>
    <div class="row">
        <button type="submit">Submit</button>
    </div>
</form>
<a href="${createLink(action:'index')}" class="button">Catalog</a>
</body>