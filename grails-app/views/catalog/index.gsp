<head>
    <meta name="layout" content="main"/>
    <title>Book catalog</title>
</head>
<body>
<h1>Catalog</h1>
<table id="catalog-table" class="u-full-width">
    <thead>
    <tr>
        <th>ISBN</th>
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
        <th>Stock</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${books}">
        <tr>
            <td>${it.isbn}</td>
            <td>${it.title}</td>
            <td>${it.description}</td>
            <td>${it.price}</td>
            <td>${it.stock}</td>
            <td><a href="${createLink(controller: 'cart', action: 'add', params: [isbn: it.isbn])}">Add to cart</a></td>
        </tr>
    </g:each>
    </tbody>
</table>
<a class="button" href="${createLink(action:'addBook')}">New book</a>
</body>