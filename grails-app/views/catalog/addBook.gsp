<head>
    <meta name="layout" content="main"/>
    <title>Add a new book</title>
</head>
<body>
<h1>New book</h1>
<g:if test="${success}">
    <div class="success-message">
        <h2>Book successfully added</h2>
    </div>
</g:if>
<g:if test="${failure}">
    <div class="failure-message">
        <h2>Invalid request</h2>
    </div>
</g:if>
<form id="addBookForm" method="post">
    <label for="isbn">ISBN</label>
    <input type="text" name="isbn" id="isbn"/>
    <label for="title">Title</label>
    <input type="text" name="title" id="title"/>
    <label for="description">Description</label>
    <input type="text" name="description" id="description"/>
    <label for="price">Price</label>
    <input type="text" name="price" id="price"/>
    <div class="row">
        <button type="submit">Submit</button>
    </div>
</form>
<a href="${createLink(action:'index')}" class="button">Catalog</a>
</body>