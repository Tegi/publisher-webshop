<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>
        <g:layoutTitle/>
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <asset:stylesheet href="normalize.css"/>
    <asset:stylesheet href="skeleton.css"/>
    <asset:stylesheet href="app.css"/>
    <g:layoutHead/>
</head>
<body>
<div class="container">
    <cart:renderCart/>
    <hr/>
    <g:layoutBody/>
</div>
</body>
</html>
