package hu.tegi

import com.github.pgasync.ConnectionPool
import com.github.pgasync.ConnectionPoolBuilder
import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import hu.tegi.asyncdb.pg.PooledPgDatabaseContext
import hu.tegi.bookshop.Catalog
import hu.tegi.bookshop.Webshop
import hu.tegi.bookshop.pg.PgCatalog
import org.apache.catalina.connector.Connector
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory
import org.springframework.context.annotation.Bean

class Application extends GrailsAutoConfiguration implements EmbeddedServletContainerCustomizer, TomcatConnectorCustomizer {

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    @Bean
    Catalog catalog() {
        return new PgCatalog(getBean(PooledPgDatabaseContext))
    }

    @Bean
    PooledPgDatabaseContext pooledPgDatabaseContext() {
        ConnectionPoolBuilder builder = new ConnectionPoolBuilder()
        ConnectionPool pool = builder.hostname('127.0.0.1')
                .username('postgres')
                .password('postgres')
                .database('publisher_webshop')
                .poolSize(5000)
                .build()
        return new PooledPgDatabaseContext(DSL.using(SQLDialect.POSTGRES_9_5), builder.build())
    }

    @Bean
    Webshop webshop() {
        return new Webshop(getBean(PooledPgDatabaseContext))
    }

    private <T> T getBean(Class<T> clazz) {
        return applicationContext.getBean(clazz)
    }

    private <T> T getBean(String name, Class<T> type) {
        return applicationContext.getBean(name, type)
    }

    @Override
    void customize(ConfigurableEmbeddedServletContainer container) {
        TomcatEmbeddedServletContainerFactory tomcatContainer = (TomcatEmbeddedServletContainerFactory) container
        tomcatContainer.tomcatConnectorCustomizers.add(this)
    }

    @Override
    void customize(Connector connector) {
        connector.setAttribute('maxThreads', 8)
        connector.setAttribute('maxConnections', -1)
        connector.setAttribute('acceptCount', 10000)
        connector.setAttribute('processorCache', 10000)
    }
}